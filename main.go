// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"compress/gzip"
	"context"
	"encoding/json"
	"flag"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/gorilla/securecookie"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

type App struct {
	mongo                 *mongo.Client
	dbName                string
	dbTimeout             time.Duration
	defaultCollectionName string
	indexTmpl             *template.Template
	notesPerPage          int
}

type Note struct {
	ID         string    `json:"id" bson:"_id"`
	Ciphertext string    `json:"ciphertext"`
	Created    time.Time `json:"created"`
	Tags       []string  `json:"tags"`
	Marks      string    `json:"marks"`
}

func (app *App) indexHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	collectionName := r.FormValue("c")
	if collectionName == "" {
		http.Redirect(w, r, "/?c="+app.defaultCollectionName, http.StatusSeeOther)
		return
	}
	collection := app.mongo.Database(app.dbName).Collection(collectionName)

	opts := options.Find().
		SetSort(bson.D{{"_id", -1}}).
		SetLimit(int64(app.notesPerPage) + 1)

	query := bson.M{}
	before := r.FormValue("before")
	if before != "" {
		objID, _ := primitive.ObjectIDFromHex(before)
		query = bson.M{"_id": bson.M{"$lt": objID}}
	}

	ctx, cancel := context.WithTimeout(r.Context(), app.dbTimeout)
	defer cancel()
	cur, err := collection.Find(ctx, query, opts)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	defer cur.Close(ctx)

	var notes []Note
	if err := cur.All(ctx, &notes); err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	newBefore := ""
	if len(notes) > app.notesPerPage {
		notes = notes[:app.notesPerPage]
		newBefore = notes[app.notesPerPage-1].ID
	}

	if err := app.indexTmpl.Execute(w, struct {
		Notes      []Note
		Before     string
		Collection string
	}{
		notes,
		newBefore,
		collectionName,
	}); err != nil {
		log.Println(err)
	}
}

func (app *App) addNote(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	collectionName := r.FormValue("c")
	if collectionName == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	collection := app.mongo.Database(app.dbName).Collection(collectionName)

	var note Note
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	tags := note.Tags[:0]
	for _, tag := range note.Tags {
		tag = strings.TrimSpace(tag)
		if tag == "" {
			continue
		}
		tags = append(tags, tag)
	}
	note.Tags = tags

	ctx, cancel := context.WithTimeout(r.Context(), app.dbTimeout)
	defer cancel()
	res, err := collection.InsertOne(ctx, bson.M{
		"ciphertext": note.Ciphertext,
		"tags":       note.Tags,
		"created":    primitive.NewDateTimeFromTime(time.Now()),
	})
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(struct {
		ID string `json:"id"`
	}{
		res.InsertedID.(primitive.ObjectID).Hex(),
	}); err != nil {
		log.Println(err)
	}
}

func (app *App) addTag(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	collectionName := r.FormValue("c")
	if collectionName == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	collection := app.mongo.Database(app.dbName).Collection(collectionName)

	var postData struct {
		ID  string
		Tag string
	}
	if err := json.NewDecoder(r.Body).Decode(&postData); err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	postData.Tag = strings.TrimSpace(postData.Tag)
	if postData.Tag == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	objID, err := primitive.ObjectIDFromHex(postData.ID)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), app.dbTimeout)
	defer cancel()
	if _, err := collection.UpdateOne(ctx, bson.D{{"_id", objID}}, bson.D{{
		"$addToSet", bson.D{{"tags", postData.Tag}},
	}}); err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (app *App) removeTag(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	collectionName := r.FormValue("c")
	if collectionName == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	collection := app.mongo.Database(app.dbName).Collection(collectionName)

	var postData struct {
		ID  string
		Tag string
	}
	if err := json.NewDecoder(r.Body).Decode(&postData); err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	objID, err := primitive.ObjectIDFromHex(postData.ID)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), app.dbTimeout)
	defer cancel()
	if _, err := collection.UpdateOne(ctx, bson.D{{"_id", objID}}, bson.D{{
		"$pullAll", bson.D{{"tags", bson.A{postData.Tag}}},
	}}); err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (app *App) updateMarks(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	collectionName := r.FormValue("c")
	if collectionName == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	collection := app.mongo.Database(app.dbName).Collection(collectionName)

	var postData struct {
		ID         string
		Ciphertext string
	}
	if err := json.NewDecoder(r.Body).Decode(&postData); err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	objID, err := primitive.ObjectIDFromHex(postData.ID)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithTimeout(r.Context(), app.dbTimeout)
	defer cancel()
	if _, err := collection.UpdateOne(ctx, bson.D{{"_id", objID}}, bson.D{{
		"$set", bson.D{{"marks", postData.Ciphertext}},
	}}); err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (app *App) backupCollection(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	collectionName := r.FormValue("c")
	if collectionName == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	collection := app.mongo.Database(app.dbName).Collection(collectionName)

	ctx := r.Context()
	cur, err := collection.Find(ctx, bson.D{})
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	defer cur.Close(ctx)

	gw := gzip.NewWriter(w)
	w.Header().Set("Content-Encoding", "gzip")
	w.Header().Set("Content-Type", "application/json")
	_, _ = gw.Write([]byte{'['})
	first := true
	enc := json.NewEncoder(gw)
	for cur.Next(ctx) {
		var note Note
		if err := cur.Decode(&note); err != nil {
			log.Println(err)
			break
		}
		if first {
			first = false
		} else {
			_, _ = gw.Write([]byte{','})
		}
		if err := enc.Encode(note); err != nil {
			log.Println(err)
			break
		}
	}
	if err := cur.Err(); err != nil {
		log.Println(err)
	}
	_, _ = gw.Write([]byte{']'})
	gw.Close()
}

var (
	hashKey      = securecookie.GenerateRandomKey(64)
	secureCookie = securecookie.New(hashKey, nil)
)

func authMiddleware(username, password string) func(http.HandlerFunc) http.HandlerFunc {
	if password == "" {
		return func(h http.HandlerFunc) http.HandlerFunc { return h }
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}

	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			authenticated := false
			if cookie, err := r.Cookie("auth"); err == nil {
				value := make(map[string]string)
				if err := secureCookie.Decode("auth", cookie.Value, &value); err == nil && value["user-agent"] == r.UserAgent() {
					authenticated = true
				}
			}

			if !authenticated {
				user, pass, ok := r.BasicAuth()
				if !ok || bcrypt.CompareHashAndPassword(hashedPassword, []byte(pass)) != nil || user != username {
					w.Header().Add("Www-Authenticate", `Basic realm="Who are you?"`)
					w.WriteHeader(http.StatusUnauthorized)
					return
				}

				if encoded, err := secureCookie.Encode("auth", map[string]string{
					"user-agent": r.UserAgent(),
				}); err == nil {
					cookie := &http.Cookie{
						Name:     "auth",
						Value:    encoded,
						Path:     "/",
						HttpOnly: true,
					}
					if r.Proto == "https" {
						cookie.Secure = true
					}
					http.SetCookie(w, cookie)
				}
			}

			next(w, r)
		}
	}
}

func main() {
	addr := flag.String("http", ":3000", "HTTP service address")
	dbTimeout := flag.Duration("timeout", 15*time.Second, "Default timeout for database operations")
	notesPerPage := flag.Int("n", 12, "Number of notes per page")
	defaultCollectionName := flag.String("c", "notes", "Default MongoDB collection name")
	password := flag.String("p", "", "Protect with a password")
	flag.Parse()
	if flag.NArg() > 0 {
		log.Fatal("no non-flag arguments are expected")
	}

	indexTmpl, err := template.ParseFiles("index.html")
	if err != nil {
		log.Fatal(err)
	}

	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		log.Fatal("MONGODB_URI environment variable is empty.")
	}

	parsedURI, err := url.Parse(uri)
	if err != nil {
		log.Fatal("invalid URI:", uri)
	}
	dbName := strings.TrimPrefix(parsedURI.Path, "/")

	ctx, cancel := context.WithTimeout(context.Background(), *dbTimeout)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}
	if err := client.Ping(ctx, nil); err != nil {
		log.Fatal(err)
	}
	cancel()

	app := App{
		indexTmpl:             indexTmpl,
		mongo:                 client,
		dbName:                dbName,
		dbTimeout:             *dbTimeout,
		notesPerPage:          *notesPerPage,
		defaultCollectionName: *defaultCollectionName,
	}

	withAuth := authMiddleware("paste", *password)

	http.HandleFunc("/", withAuth(app.indexHandler))
	http.HandleFunc("/add", withAuth(app.addNote))
	http.HandleFunc("/tag/add", withAuth(app.addTag))
	http.HandleFunc("/tag/remove", withAuth(app.removeTag))
	http.HandleFunc("/marks/update", withAuth(app.updateMarks))
	http.HandleFunc("/backup", withAuth(app.backupCollection))
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))
	log.Fatal(http.ListenAndServe(*addr, nil))
}
