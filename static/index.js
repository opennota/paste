/* global sjcl, collection */
'use strict';
document.addEventListener(
  'DOMContentLoaded',
  () => {
    const passwordInput = document.querySelector('#password');
    passwordInput.addEventListener('change', decrypt);
    passwordInput.addEventListener('paste', decrypt);
    passwordInput.addEventListener('keypress', decrypt);
    passwordInput.addEventListener('blur', () => {
      localStorage.password = passwordInput.value;
    });
    let timer = null;
    if (localStorage.password) {
      passwordInput.value = localStorage.password;
      decrypt();
    }
    function updateDefaultTag(e) {
      localStorage.defaultTag = e.target.value;
    }
    const defaultTagInput = document.querySelector('.input-default-tag');
    defaultTagInput.addEventListener('change', updateDefaultTag);
    defaultTagInput.addEventListener('paste', updateDefaultTag);
    defaultTagInput.addEventListener('keypress', updateDefaultTag);
    if (localStorage.defaultTag) {
      defaultTagInput.value = localStorage.defaultTag;
    }
    const pasteBtn = document.querySelector('.action-paste');
    pasteBtn.addEventListener('click', paste);
    document.querySelector('.action-backup').addEventListener('click', backup);
    const actionSave = document.querySelector('.action-save');
    actionSave.addEventListener('click', saveUnsaved);
    let unsaved = [];

    function setMenuButtonState(btn, open) {
      if (open) {
        btn.classList.remove('icon-menu');
        btn.classList.add('icon-cancel');
      } else {
        btn.classList.remove('icon-cancel');
        btn.classList.add('icon-menu');
      }
    }

    document.querySelector('.dropdown-toggle').addEventListener('change', e => {
      const toggle = e.target;
      const btn = toggle.nextElementSibling;
      const closeMenuOnClick = e2 => {
        if (ancestorByClass(e2.target, 'no-close-on-click')) return;
        toggle.checked = false;
        setMenuButtonState(btn, toggle.checked);
        document.removeEventListener('click', closeMenuOnClick);
        if (e2.target === btn) e2.preventDefault();
      };
      setMenuButtonState(btn, toggle.checked);
      if (toggle.checked) document.addEventListener('click', closeMenuOnClick);
    });

    const replacements = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
    };
    function replace(c) {
      return replacements[c];
    }
    function escapeHtml(html) {
      return html.replace(/[&<>]/g, replace);
    }
    function setText(note, text) {
      const escaped = escapeHtml(text);
      note.querySelector('.note-content').innerHTML = escaped;
    }

    function decrypt() {
      localStorage.password = passwordInput.value;
      if (!passwordInput.value) return;

      if (timer) {
        clearInterval(timer);
        timer = null;
      }

      const encrypted = Array.from(
        document.querySelectorAll('.note-encrypted')
      );
      if (encrypted.length) {
        timer = setInterval(() => {
          if (!encrypted.length) {
            clearInterval(timer);
            timer = null;
            return;
          }
          const note = encrypted.shift();
          const password = passwordInput.value;
          const ciphertext = note.getAttribute('data-ciphertext');
          const marksCiphertext = note.getAttribute('data-marks');
          try {
            const text = sjcl.decrypt(password, ciphertext);
            setText(note, text);
            note.classList.remove('note-encrypted');
            note.removeAttribute('data-ciphertext');
            note.removeAttribute('data-marks');
            if (marksCiphertext) {
              const marks = JSON.parse(sjcl.decrypt(password, marksCiphertext));
              if (marks && marks.length)
                note.querySelector('.note-content').innerHTML = mark(
                  text,
                  marks
                );
            }
          } catch (e) {
            //
          }
        }, 1);
      }
    }

    function showError(err) {
      const el = document.querySelector('.errdiv');
      el.querySelector('.error').textContent = err;
      el.classList.remove('hidden');
    }

    function paste() {
      const password = passwordInput.value;
      if (!password) {
        showError('Enter a password first.');
        passwordInput.focus();
        return;
      }

      scrollTo(0, 0);
      document.querySelector('.errdiv').classList.add('hidden');
      navigator.permissions
        .query({ name: 'clipboard-read' })
        .then(result => {
          if (result.state === 'prompt' || result.state === 'granted')
            navigator.clipboard
              .readText()
              .then(add)
              .catch(showError);
        })
        .catch(showError);
    }

    let lastText;
    function add(text) {
      if (!text.trim()) {
        showError('The clipboard is empty.');
        return;
      }
      if (text === lastText) {
        showError('This text is already pasted.');
        return;
      }
      lastText = text;

      const defaultTag = defaultTagInput.value.trim();
      const note = addOnClient(text, defaultTag);
      const password = passwordInput.value;
      const ciphertext = sjcl.encrypt(password, text);
      addOnServer(note, ciphertext, defaultTag);
    }

    function addOnClient(text, tag) {
      const note = document.createElement('div');
      note.setAttribute('class', 'note');
      note.innerHTML =
        `<div class="note-header ${tag ? '' : 'hidden'}">
            <i class="icon-tag action-new-tag -clickable"></i>
            <div class="clearable-input hidden">
              <input type="text" class="input-add-tag" size="10"></input>
              <i class="icon-cancel-circled icon-clear-input"></i>
            </div>
            <div class="note-tags">` +
        (tag ? '<span class="tag">' + escapeHtml(tag) + '</span>' : '') +
        `</div>
          </div>
          <div class="note-content"></div>
          <div class="note-actions ${tag ? 'hidden' : ''}">
            <i class="action-show-note-header icon-tag -clickable"></i>
          </div>
        </div>`;
      setText(note, text);
      const notes = document.querySelector('.notes');
      notes.insertBefore(note, notes.firstChild);
      return note;
    }

    const defaultFetchOptions = {
      mode: 'no-cors',
      cache: 'no-store',
      credentials: 'same-origin',
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
    };

    function httpGet(url) {
      return fetch(url, {
        method: 'GET',
        ...defaultFetchOptions,
      });
    }

    function httpPost(url, body) {
      return fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body,
        ...defaultFetchOptions,
      });
    }

    let lastSelectionChanged;
    function addOnServer(note, ciphertext, tag, cb) {
      httpPost(
        '/add?c=' + encodeURIComponent(collection),
        JSON.stringify({
          ciphertext,
          tags: tag ? [tag] : [],
        })
      )
        .then(response => {
          if (!response.ok || response.status != 200)
            throw `request failed; status code ${response.status}`;
          return response.json();
        })
        .then(json => {
          note.setAttribute('data-id', json.id);
          if (lastSelectionChanged)
            document.dispatchEvent(lastSelectionChanged);
          if (tag) note.querySelector('.tag').classList.add('tag--applied');
          if (cb) cb(true, note, json.id);
        })
        .catch(err => {
          if (cb) cb(false);
          showError(err);
          if (!unsaved.some(x => x.note === note))
            unsaved.push({ note, ciphertext, tag });
          ancestorByClass(actionSave, 'save-button-div').classList.remove(
            'hidden'
          );
        });
    }

    function saveUnsaved() {
      const password = passwordInput.value;
      if (!password) return;

      actionSave.setAttribute('disabled', true);
      let n = unsaved.length;
      let allOk = true;
      const cb = (ok, note, id) => {
        if (ok) {
          note.setAttribute('data-id', id);
          unsaved = unsaved.filter(x => x.note !== note);
        }
        allOk = allOk && ok;
        n--;
        if (!n) {
          actionSave.removeAttribute('disabled');
          ancestorByClass(actionSave, 'save-button-div').classList.add(
            'hidden'
          );
          if (allOk) document.querySelector('.errdiv').classList.add('hidden');
        }
      };
      for (let u of unsaved.slice()) {
        addOnServer(u.note, u.ciphertext, u.tag, cb);
      }
    }

    function csvEscape(text) {
      if (!/[",\n\r]/.test(text)) return text;
      return '"' + text.replace(/"/g, '""') + '"';
    }

    function downloadAsCsv(notes) {
      const password = passwordInput.value;
      const parts = new Array(notes.length);
      for (let i = 0; i < notes.length; i++) {
        const note = notes[i];
        let text = '';
        let marks = '';
        try {
          text = sjcl.decrypt(password, note.ciphertext);
          marks = sjcl.decrypt(password, note.marks);
        } catch (e) {
          //
        }
        parts[i] = `${note.created},${note.id},${csvEscape(text)},${
          note.tags && note.tags.length
            ? csvEscape(JSON.stringify(note.tags))
            : ''
        },${csvEscape(marks)}\n`;
      }
      const blob = new Blob(parts, { type: 'text/csv' });
      const url = URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      const dateStr = new Date().toISOString().replace(/\D/g, '');
      link.download = `${collection}-${dateStr}-${notes.length}.csv`;
      link.click();
      URL.revokeObjectURL(url);
      link.remove();
    }

    function backup() {
      httpGet('/backup?c=' + encodeURIComponent(collection))
        .then(response => {
          if (!response.ok || response.status != 200)
            throw `request failed; status code ${response.status}`;
          return response.json();
        })
        .then(downloadAsCsv)
        .catch(showError);
    }

    function ancestorByClass(el, cls) {
      let p = el;
      while (p) {
        if (p.classList && p.classList.contains(cls)) break;
        p = p.parentNode;
      }
      return p;
    }

    function hideHeaderIfNeeded(note) {
      if (
        !note.querySelector('.tag') &&
        note.querySelector('.clearable-input.hidden')
      ) {
        note.querySelector('.note-header').classList.add('hidden');
        note.querySelector('.note-actions').classList.remove('hidden');
      }
    }

    document.addEventListener('click', e => {
      const el = e.target;

      if (el.classList.contains('icon-clear-input')) {
        const input = ancestorByClass(el, 'clearable-input').querySelector(
          'input'
        );
        input.value = '';
        input.focus();
      }

      if (el.classList.contains('action-new-tag')) {
        const inputDiv = el.nextElementSibling;
        inputDiv.classList.toggle('hidden');
        if (inputDiv.classList.contains('hidden'))
          hideHeaderIfNeeded(ancestorByClass(el, 'note'));
        else {
          const input = inputDiv.querySelector('.input-add-tag');
          input.value = defaultTagInput.value;
          input.focus();
          input.select();
        }
      }

      if (el.classList.contains('action-show-note-header')) {
        const note = ancestorByClass(el, 'note');
        const noteHeader = note.querySelector('.note-header');
        noteHeader.classList.remove('hidden');
        note.querySelector('.note-actions').classList.add('hidden');
        noteHeader.querySelector('.action-new-tag').click();
      }

      if (el.classList.contains('tag')) {
        const tag = el.textContent;
        const note = ancestorByClass(el, 'note');
        if (note && note.hasAttribute('data-id')) {
          document.querySelector('.errdiv').classList.add('hidden');
          if (el.classList.contains('tag--applied'))
            removeTagOnServer(note.getAttribute('data-id'), tag, el);
          else {
            el.classList.remove('tag--deleted');
            addTagOnServer(note.getAttribute('data-id'), tag, el);
          }
        }
      }
    });

    document.addEventListener('keydown', e => {
      const el = e.target;
      if (!el.classList.contains('input-add-tag')) return;
      if (e.which != 13) return;
      document.querySelector('.errdiv').classList.add('hidden');
      const tag = el.value.trim();
      if (!tag) return;
      const note = ancestorByClass(el, 'note');
      if (!note || !note.hasAttribute('data-id')) return;
      const tags = note.querySelector('.note-tags');
      let tagEl;
      for (let t of tags.querySelectorAll('.tag')) {
        if (t.textContent === tag) {
          tagEl = t;
          break;
        }
      }
      if (!tagEl) {
        tagEl = document.createElement('span');
        tagEl.className = 'tag';
        tagEl.textContent = tag;
        tags.appendChild(tagEl);
      }
      if (!tagEl.classList.contains('tag--applied')) {
        tagEl.classList.remove('tag--deleted');
        addTagOnServer(note.getAttribute('data-id'), tag, tagEl);
      }
      el.value = '';
      ancestorByClass(el, 'clearable-input').classList.add('hidden');
    });

    function addTagOnServer(id, tag, tagEl) {
      httpPost(
        '/tag/add?c=' + encodeURIComponent(collection),
        JSON.stringify({ id, tag })
      )
        .then(response => {
          if (!response.ok || response.status != 204)
            throw `request failed; status code ${response.status}`;
          tagEl.classList.add('tag--applied');
        })
        .catch(showError);
    }

    function removeTagOnServer(id, tag, tagEl) {
      httpPost(
        '/tag/remove?c=' + encodeURIComponent(collection),
        JSON.stringify({ id, tag })
      )
        .then(response => {
          if (!response.ok || response.status != 204)
            throw `request failed; status code ${response.status}`;
          tagEl.classList.remove('tag--applied');
          tagEl.classList.add('tag--deleted');
          setTimeout(() => {
            if (tagEl.classList.contains('tag--deleted')) {
              const note = ancestorByClass(tagEl, 'note');
              tagEl.remove();
              hideHeaderIfNeeded(note);
            }
          }, 2000);
        })
        .catch(showError);
    }

    const markBtn = document.querySelector('.action-mark');
    markBtn.addEventListener('click', addMarkFromSelection);

    function selectionChanged(e) {
      lastSelectionChanged = e;
      const sel = document.getSelection();
      if (!sel || !sel.rangeCount || !sel.toString().trim().length) {
        pasteBtn.removeAttribute('disabled');
        return markBtn.classList.add('hidden');
      }
      pasteBtn.setAttribute('disabled', '');
      const c1 = ancestorByClass(sel.anchorNode, 'note-content');
      const c2 = ancestorByClass(sel.focusNode, 'note-content');
      if (
        !c1 ||
        !c2 ||
        c1 !== c2 ||
        ancestorByClass(c1, 'note').classList.contains('note-encrypted') ||
        !ancestorByClass(c1, 'note').getAttribute('data-id')
      )
        return markBtn.classList.add('hidden');
      markBtn.classList.remove('hidden');
    }

    function updateMarksOnServer(note, ciphertext, cb) {
      const id = note.getAttribute('data-id');
      httpPost(
        '/marks/update?c=' + encodeURIComponent(collection),
        JSON.stringify({ id, ciphertext })
      )
        .then(response => {
          if (!response.ok || response.status != 204)
            throw `request failed; status code ${response.status}`;
          if (cb) cb(true);
        })
        .catch(err => {
          showError(err);
          if (cb) cb(false);
        });
    }

    function regexpEscape(r) {
      return r.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    function mark(text, marks) {
      const r = new RegExp(marks.map(m => regexpEscape(m)).join('|'), 'gi');
      let i = 0;
      let result = '';
      let m;
      while ((m = r.exec(text))) {
        result += escapeHtml(text.slice(i, m.index));
        result += '<mark>';
        result += escapeHtml(m[0]);
        result += '</mark>';
        i = m.index + m[0].length;
      }
      result += escapeHtml(text.slice(i));
      return result;
    }

    function updateMarksOnClient(note, marks) {
      const content = note.querySelector('.note-content');
      content.innerHTML = mark(content.textContent, marks);
    }

    function addMarkFromSelection() {
      document.querySelector('.errdiv').classList.add('hidden');
      markBtn.classList.add('hidden');
      const sel = document.getSelection();
      if (!sel || !sel.rangeCount) return;
      const text = sel.toString().trim();
      if (!text) return;
      const note = ancestorByClass(sel.anchorNode, 'note');
      const marks = Array.prototype.map.call(
        note.querySelectorAll('mark'),
        e => e.textContent
      );
      if (marks.includes(text)) return;
      marks.push(text);
      marks.sort((a, b) => {
        const c = b.length - a.length;
        if (c) return c;
        if (a < b) return -1;
        if (a > b) return 1;
        return 0;
      });
      updateMarksOnClient(note, marks);
      const password = passwordInput.value;
      const ciphertext = sjcl.encrypt(password, JSON.stringify(marks));
      updateMarksOnServer(note, ciphertext, ok => {
        if (!ok)
          updateMarksOnClient(
            note,
            marks.filter(m => m !== text)
          );
      });
    }

    document.addEventListener('selectionchange', selectionChanged);
  },
  false
);
